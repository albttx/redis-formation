# Workshop 01

## Exercices

1. Show the list of all keys

exptected ouput:
```bash
 1) "id:481762"
 2) "id:504080"
 3) "id:244852"
 4) "id:30890"
 5) "id:533932"
 6) "id:50794"
 7) "id:497713"
 8) "id:433422"
 9) "id:273059"
10) "id:70609"
....
```

2. Show list of keys containing with "999"

exptected ouput:
```bash
1) "id:9997"
2) "id:9994"
3) "id:99920"
4) "id:9993"
5) "id:9991"
6) "id:9992"
```

3. Show all the informations for the movie with id = 218

exptected ouput:
```bash
1) "overview"
2) "In the post-apocalyptic future, reigning tyrannical supercomputers teleport a cyborg assassin known as the 'Terminator' back to 1984 to kill Sarah Connor, whose unborn son is destined to lead insurgents against 21st century mechanical hegemony. Meanwhile, the human-resistance movement dispatches a lone warrior to safeguard Sarah. Can he stop the virtually indestructible killing machine?"
3) "poster"
4) "https://image.tmdb.org/t/p/w1280/q8ffBuxQlYOHrvPniLgCbmKK4Lv.jpg"
5) "title"
6) "The Terminator"
7) "release_date"
8) "467596800"
```

4. Show the movie title for the movie with id = 218

exptected ouput:
```bash
"The Terminator"
```

5. Delete the key 218 and recreate it

```bash
$ DELETE COMMAND
(integer 1)

$ RECREATE COMMAND
(integer 4)

$ GET COMMAND
1) "overview"
2) "In the post-apocalyptic future, reigning tyrannical supercomputers teleport a cyborg assassin known as the 'Terminator' back to 1984 to kill Sarah Connor, whose unborn son is destined to lead insurgents against 21st century mechanical hegemony. Meanwhile, the human-resistance movement dispatches a lone warrior to safeguard Sarah. Can he stop the virtually indestructible killing machine?"
3) "poster"
4) "https://image.tmdb.org/t/p/w1280/q8ffBuxQlYOHrvPniLgCbmKK4Lv.jpg"
5) "title"
6) "The Terminator"
7) "release_date"
8) "467596800"
```

6. Create a sorted list named `movies:harry_potter` containg the list of all Harry Potter movies (BONUS: by order)

```bash
# NOTE(albttx): How to get the harry potter list
$ cat movies.json| jq -r '' | grep -i -B 1 -E "title.*Harry Potter"
...

671:   "Harry Potter and the Philosopher's Stone"
767:   "Harry Potter and the Half-Blood Prince"
672:   "Harry Potter and the Chamber of Secrets"
12445: "Harry Potter and the Deathly Hallows: Part 2"
12444: "Harry Potter and the Deathly Hallows: Part 1"
673:   "Harry Potter and the Prisoner of Azkaban"
675:   "Harry Potter and the Order of the Phoenix"
674:   "Harry Potter and the Goblet of Fire"
```

expected output:

```bash
$ ADD COMMAND
(integer) 8

$ redis-cli ZRANGE movies:harry_potter 0 -1
1) "id:671"
2) "id:672"
3) "id:673"
4) "id:674"
5) "id:675"
6) "id:767"
7) "id:12444"
8) "id:12445"
```