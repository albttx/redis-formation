# Workshops

## Setup

- Start redis server

```bash
$ docker-compose up -d
```

## Workshops

- [Workshop 01](./WORKSHOP_01.md)
- [Workshop 02](./WORKSHOP_02.md)
- [Workshop 03](./WORKSHOP_03.md)
- [Workshop 04](./WORKSHOP_04.md)