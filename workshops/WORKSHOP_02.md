# Workshop 02

## Setup

- Install jq

```bash
$ sudo apt install jq
```

## Exercices


1. Create a lua script that get all ids in a sorted set

```bash
$ redis-cli --eval ./create_ids_list.lua
(nil)

$ redis-cli ZRANGE ids 0 5
1) "2"
2) "3"
3) "5"
4) "6"
5) "11"
6) "12"
```

2. Create a Lua scripts to get all movies by title

```bash
$ redis-cli --eval ./get_movies.lua "Harry Potter" | jq
{
  "id:12445": {
    "title": "Harry Potter and the Deathly Hallows: Part 2",
    "overview": "Harry, Ron and Hermione continue their quest to vanquish the evil Voldemort once and for all. Just as things begin to look hopeless for the young wizards, Harry discovers a trio of magical objects that endow him with powers to rival Voldemort's formidable skills.",
    "release_date": "1310000400",
    "poster": "https://image.tmdb.org/t/p/w1280/fTplI1NCSuEDP4ITLcTps739fcC.jpg"
  },
  "id:767": {
    "title": "Harry Potter and the Half-Blood Prince",
    "overview": "As Harry begins his sixth year at Hogwarts, he discovers an old book marked as 'Property of the Half-Blood Prince', and begins to learn more about Lord Voldemort's dark past.",
    "release_date": "1246928400",
    "poster": "https://image.tmdb.org/t/p/w1280/bFXys2nhALwDvpkF3dP3Vvdfn8b.jpg"
  },
  "id:673": {
    "title": "Harry Potter and the Prisoner of Azkaban",
    "overview": "Harry, Ron and Hermione return to Hogwarts for another magic-filled year. Harry comes face to face with danger yet again, this time in the form of escaped convict, Sirius Black—and turns to sympathetic Professor Lupin for help.",
    "release_date": "1085965200",
    "poster": "https://image.tmdb.org/t/p/w1280/jUFjMoLh8T2CWzHUSjKCojI5SHu.jpg"
  },
...
}
```
