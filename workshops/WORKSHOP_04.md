# Workshop 04

## Setup

```bash
cd 04
docker-compose up -d

# open 3 shell
docker-compose exec redis01 sh
docker-compose exec redis02 sh
docker-compose exec redis03 sh
```

## Exercices

1. Setup the configuration file to have a master/slave replication

```bash
$ redis-cli -p 6301
127.0.0.1:6301> SET foo bar
OK

$ redis-cli -p 6302 
127.0.0.1:6302> GET foo
"bar"
```

2. Setup configuration to run replication with redis-sentinel

```bash
$ redis-cli -p 6301
127.0.0.1:6301> SET foo bar
OK

$ redis-cli -p 6302 
127.0.0.1:6302> GET foo
"bar"
```

3. Create a user that have only read-access to the values

