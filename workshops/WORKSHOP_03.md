# Workshop 02

## Setup

- Install Go or/and python

- Install httpie

```bash
sudo apt install httie
```

## Exercices

1. Create an API to get, create and delete movies

- Get
```bash
$ http GET "localhost:3000/movies?id=660"
HTTP/1.1 200 OK
Content-Length: 401
Content-Type: application/json; charset=UTF-8
Date: Mon, 15 Jun 2020 16:17:52 GMT

{
    "movie": {
        "overview": "A criminal organization has obtained two nuclear bombs and are asking for a 100 million pound ransom in the form of diamonds in seven days or they will use the weapons. The secret service sends James Bond to the Bahamas to once again save the world.",
        "poster": "https://image.tmdb.org/t/p/w1280/sASN1VnJxWosdzp4mH40P47Xhhz.jpg",
        "release_date": "-127699200",
        "title": "Thunderball"
    }
}
```

- Create
```bash
$ http POST localhost:3000/movies title=foo overview=foobar
HTTP/1.1 201 Created
Content-Length: 16
Content-Type: application/json; charset=UTF-8
Date: Mon, 15 Jun 2020 16:19:55 GMT

{
    "id": "594081"
}
```

- Delete

```bash
$ http DELETE "localhost:3000/movies?id=100"
HTTP/1.1 200 OK
Content-Length: 3
Content-Type: application/json; charset=UTF-8
Date: Mon, 15 Jun 2020 16:20:53 GMT

{}
```

2. Add a middleware that will count every API calls and add a route `/stats`

```bash
$ http GET "localhost:3000/stats"
HTTP/1.1 200 OK
Content-Length: 37
Content-Type: application/json; charset=UTF-8
Date: Mon, 15 Jun 2020 16:22:12 GMT

{
    "DELETE": "12",
    "GET": "42",
    "POST": "5"
}
```

3. Write unit tests (use mocking API)

```bash
$ go test -v ./...
=== RUN   TestCreateMovies
--- PASS: TestCreateMovies (0.00s)
=== RUN   TestGetMovies
--- PASS: TestGetMovies (0.00s)
PASS
ok  	github.com/albttx/redis-formation/workshops/03	0.003s
```
