#!/usr/bin/env python3

import json
import redis

r = redis.Redis()
r.flushdb()

with open('movies.json') as f:
    for movie in json.load(f):
        movie_id = movie['id']
        movie.pop('id')
        r.hmset('id:' + movie_id, movie)
f.close()