#!/bin/sh

function seed() {
    sleep 5
    ./load_data.py
}

seed &

redis-server $@