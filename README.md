
# PROGRAMME DE LA FORMATION REDIS

# Slides

[google slides link](https://docs.google.com/presentation/d/1dCzs56kbf-lubyihFdTX2WOMcHtfQA9OiZlI_mnkBCU/edit?usp=sharing)

## Pré-requis

- Installer [docker](https://docker.com) et [docker-compose](https://docs.docker.com/compose/)

```bash
# Telecharger Docker
$ curl -fsSL https://get.docker.com -o get-docker.sh
$ sh get-docker.sh

# Installer docker-compose
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose

# BONUS: Ajout de l'user actuel dans le groupe docker pour utilisation sans sudo
$ sudo usermod -aG docker $USER
```

- Installer [redis-cli]()

```bash
sudo apt install redis-tools
```

## INTRODUCTION À REDIS
- Redis, cache en mémoire
- Positionnement vis-à-vis des autres moteurs NoSQL
- Quand utiliser Redis, quand ne pas l’utiliser
- Les grandes références de Redis
 

## L’ARCHITECTURE REDIS
- Protocole de communication et format de données
- Atomicité des opérations
- Processus de démarrage
- Event loop & les différents événements
- Durabilité des données
- Réplication Master-Slave


## PRINCIPALES STRUCTURES DE DONNÉES ET MANIPULATION
- String, List, Set, Hash et Sorted Set
- Les principales commandes associées


## DÉVELOPPER AVEC REDIS
- Les langages d’accès client
- Les APIs en détail avec Go
- Gestion des transactions
- Des scripts côté serveur avec Lua
- Mocking Redis avec [miniredis](https://github.com/alicebob/miniredis)


## HAUTE DISPONIBILITÉ ET CLUSTERING
- Fonctionnement des réplicas et cycle de vie des données
- Clustering Redis
- Haute Disponibilité avec Redis Sentinel
 

## OPTIMISATION DES DONNÉES
- Expiration des données
- Pipelining & Multiple Argument commands
- Logical Types vs Physical Types
- Patterns appliqués à la conception de données
 

## STRUCTURES ET ORGANISATION AVANCÉE DES DONNÉES
- Publish/Subscribe
- HyperLogLog
- BitMap
- Les problématiques de requêtes complexes
 

## MONITORING DU TRAFIC
- La commande “monitor”
- Analyse des événements et History
- Les différents outils du marché


## POUR ALLER PLUS LOIN (MODULE COMPLÉMENTAIRE)
- Sécurité et Encryption
- Les principales recommandations de conception et de configuration
- Étendre Redis avec les Redis Module
- Savoir debugger ses scripts Python ou Lua
- Les principales topologies de déploiement chez les grandes références de Redis
 

## AU-DELÀ DE REDIS (MODULE COMPLÉMENTAIRE)
- Ce qu’il n’y a pas dans Redis
- La solution Entreprise Redis Labs
- Les alternatives & les potentiels successeurs
